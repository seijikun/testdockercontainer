#include <iostream>

int main() {
    #pragma omp parallel for
    for(size_t i = 0; i < 10; ++i) {
        std::cout << "Hello World" << std::endl;
    }
    
    return 0;
}
