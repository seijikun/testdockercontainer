FROM opensuse/tumbleweed:latest

RUN zypper install -y cmake git patterns-devel-C-C++-devel_C_C++ ccache clang clang-tools \
	libstdc++-devel libc++-devel libsvm-devel eigen3-devel gtest libomp-devel boost-devel

ADD main.cpp /tmp/main.cpp

RUN cd / && find -name "*pthread*" && find /lib64 -xtype l && find /usr/lib64 -xtype l \
	&& clang++ /tmp/main.cpp -lpthread -fopenmp -v -o /tmp/main
